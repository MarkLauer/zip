import os
from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))


class Config(object):
    SECRET_KEY = os.environ.get(
        'SECRET_KEY') or b'\x96\x8f\xf8\xb7\xc7P/\x9d\xd1\x01\x1b\x1f\xde!48'
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        'DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'zip.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
