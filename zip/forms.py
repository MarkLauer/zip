from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    PasswordField,
    SubmitField,
    TextAreaField,
    SelectField,
    FileField
)
from wtforms.validators import DataRequired, Length, Optional


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Sign In')


class ItemForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    description = TextAreaField(
        'Description',
        validators=[Length(min=0, max=140)]
    )
    status = SelectField(
        'Status',
        choices=[
            ('none', 'None'),
            ('sold_out', 'Sold out'),
            ('special', 'Special'),
            ('new', 'New')
        ]
    )
    image = FileField('Image')
    category_id = SelectField('Category', coerce=int, validators=[Optional()])
    brand_id = SelectField('Brand', coerce=int, validators=[Optional()])
    submit = SubmitField('Submit')


class CategoryForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    submit = SubmitField('Submit')


class BrandForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    image = FileField('Image')
    submit = SubmitField('Submit')
