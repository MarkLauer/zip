from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager

from config import Config

db = SQLAlchemy()
migrate = Migrate()
login = LoginManager()


def create_app():
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(Config)

    # flask extensions initialization
    db.init_app(app)
    migrate.init_app(app, db)
    login.init_app(app)
    login.login_view = 'admin.login'
    login.login_message = None

    from .models import Item, Category, User, Brand

    @app.shell_context_processor
    def make_shell_context():
        return {
            'db': db,
            'Item': Item,
            'Category': Category,
            'User': User,
            'Brand': Brand
        }

    # blueprints registration
    from . import catalog
    app.register_blueprint(catalog.bp)
    app.add_url_rule('/', endpoint='index')

    from . import admin
    app.register_blueprint(admin.bp)

    # errors registration
    from . import errors
    app.register_error_handler(403, errors.forbidden)
    app.register_error_handler(404, errors.page_not_found)
    app.register_error_handler(500, errors.internal_error)

    return app
