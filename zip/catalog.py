from flask import Blueprint, render_template, abort

from .models import Item, Category, Brand
from .utils import unescape_newlines

bp = Blueprint('catalog', __name__)


@bp.route('/')
def index():
    brands = Brand.query.filter(
        Brand.image.isnot(None)
    ).order_by(Brand.name).all()
    return render_template(
        'catalog/index.html',
        brands=brands
    )


@bp.route('/items/', defaults={'mode': 'brands', 'collection_id': None})
@bp.route('/items/<mode>/<int:collection_id>')
def show_item_list(mode, collection_id):
    if mode not in ('brands', 'categories'):
        abort(404)
    brands = Brand.query.order_by(Brand.name).all()
    categories = Category.query.order_by(Category.name).all()
    collections = brands if mode == 'brands' else categories
    if collection_id is None:
        collection_id = collections[0].id if collections[0] is not None else 0
    collection = next((c for c in collections if c.id == collection_id), None)
    if collection is None:
        abort(404)
    items = collection.items.order_by(Item.id).all()
    return render_template(
        'catalog/item_list.html',
        items=items,
        mode=mode,
        brands=brands,
        categories=categories,
        collection_id=collection_id,
        unescape_newlines=unescape_newlines
    )


# @bp.route('/about')
# def about():
#     return render_template('catalog/about.html')
