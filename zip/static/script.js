$(function() {
    var imageInputs = document.getElementsByClassName('image-input');
    if (imageInputs) {
        var imageInput = imageInputs[0];
    }

    if (imageInput) {
        imageInput.addEventListener('change', imagePreview, false);
    }

    function imagePreview() {
        var files = this.files;

        if (!files) {
            return;
        }
        var file = files[0];
        if (!file.type.startsWith('image/')) {
            return;
        }

        var img = document.querySelector('#imagePreview');
        img.file = file;

        var reader = new FileReader();
        reader.onload = (function(aImg) {
            return function(e) {
                aImg.src = e.target.result;
            };
        })(img);
        reader.readAsDataURL(file);
    }

    function setDeleteButtonState() {
        var checked = false;
        $.each($('#deleteForm').serializeArray(), function(i, field) {
            if (field.value) {
                checked = true;
                return;
            }
        });
        $('#deleteSubmit').attr('disabled', !checked);
    }

    $('#deleteForm').change(setDeleteButtonState);
    setDeleteButtonState();

    function goToCollection(event, url) {
        var id = event.target.value > 0 ? event.target.value : '';
        window.location.replace(
            window.location.origin + url + event.target.value
        );
    }

    $('#adminBrand').change(function(event) {
        goToCollection(event, '/admin/items/');
    });
    $('#catalogBrand').change(function(event) {
        goToCollection(event, '/items/brands/');
    });
    $('#catalogCategory').change(function(event) {
        goToCollection(event, '/items/categories/');
    });
});
