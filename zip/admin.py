from flask import (
    Blueprint,
    render_template,
    flash,
    redirect,
    url_for,
    request,
    abort
)
from flask_login import login_required, current_user, login_user, logout_user
from werkzeug.urls import url_parse

from . import db
from .models import Item, Category, User, Brand
from .forms import ItemForm, CategoryForm, LoginForm, BrandForm
from .utils import image_to_base64

bp = Blueprint('admin', __name__, url_prefix='/admin')


@bp.route('/')
@login_required
def index():
    return redirect(url_for('admin.show_item_list'))


@bp.route('/login', methods=('GET', 'POST'))
def login():
    if current_user.is_authenticated:
        return redirect(url_for('admin.index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid email or password.')
            return redirect(url_for('admin.login'))
        login_user(user, remember=True)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('admin.index')
        return redirect(next_page)
    return render_template('admin/login.html', form=form)


@bp.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('catalog.index'))


# items management
@bp.route('/items/', defaults={'brand_id': None})
@bp.route('/items/<int:brand_id>')
@login_required
def show_item_list(brand_id):
    items = Item.query.filter_by(brand_id=brand_id) if brand_id else Item.query
    items = items.outerjoin(Brand).order_by(Brand.name).all()
    brands = Brand.query.order_by(Brand.name).all()
    current_brand = Brand.query.get(brand_id) if brand_id else None
    if brand_id and not Brand.query.get(brand_id):
        abort(404)
    return render_template(
        'admin/rd_item.html',
        title='Item list',
        items=items,
        brands=brands,
        current_brand=current_brand,
        next=request.path
    )


@bp.route('/items/create', methods=('GET', 'POST'))
@login_required
def create_item():
    form = ItemForm()
    form.category_id.choices = [(c.id, c.name) for c in
                                Category.query.order_by('name')]
    form.category_id.choices.insert(0, (0, 'No category'))
    form.brand_id.choices = [(b.id, b.name) for b in
                             Brand.query.order_by('name')]
    form.brand_id.choices.insert(0, (0, 'No brand'))
    if form.validate_on_submit():
        item = Item(
            name=form.name.data,
            description=form.description.data,
            status=form.status.data,
            category_id=form.category_id.data,
            brand_id=form.brand_id.data
        )
        if form.image.data:
            item.image = image_to_base64(form.image.data, (512, 512))
        db.session.add(item)
        db.session.commit()
        flash('Item added')
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('admin.show_item_list')
        return redirect(next_page)
    return render_template(
        'admin/cud_item.html',
        title='Create Item',
        form=form
    )


@bp.route('/items/update/<int:item_id>', methods=('GET', 'POST'))
@login_required
def update_item(item_id):
    item = Item.query.get(item_id)
    if item is None:
        flash('No item with this id')
        return redirect(url_for('admin.show_item_list'))
    form = ItemForm()
    form.category_id.choices = [(c.id, c.name) for c in
                                Category.query.order_by('name')]
    form.category_id.choices.insert(0, (0, 'No category'))
    form.brand_id.choices = [(b.id, b.name) for b in
                             Brand.query.order_by('name')]
    form.brand_id.choices.insert(0, (0, 'No brand'))
    if form.validate_on_submit():
        item.name = form.name.data
        item.description = form.description.data
        item.status = form.status.data
        item.category_id = form.category_id.data
        item.brand_id = form.brand_id.data
        if form.image.data:
            item.image = image_to_base64(form.image.data, (512, 512))
        db.session.commit()
        flash('Item edited')
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('admin.show_item_list')
        return redirect(next_page)
    if request.method == 'GET':
        form.name.data = item.name
        form.description.data = item.description
        form.status.data = item.status
        form.category_id.data = item.category_id
        form.brand_id.data = item.brand_id
    return render_template(
        'admin/cud_item.html',
        title=item.name,
        form=form,
        id=item.id,
        image=item.image
    )


@bp.route('/items/delete/<int:item_id>')
@login_required
def delete_item(item_id):
    item = Item.query.get(item_id)
    if item is None:
        flash('No item with this id')
        return redirect(url_for('admin.show_item_list'))
    db.session.delete(item)
    db.session.commit()
    flash('Item deleted')
    next_page = request.args.get('next')
    if not next_page or url_parse(next_page).netloc != '':
        next_page = url_for('admin.show_item_list')
    return redirect(next_page)


@bp.route('/items/delete', methods=('POST',))
@login_required
def delete_items():
    for item_id in request.form:
        item = Item.query.get(item_id)
        if not item:
            continue
        db.session.delete(item)
    db.session.commit()
    flash('Items deleted')
    next_page = request.args.get('next')
    if not next_page or url_parse(next_page).netloc != '':
        next_page = url_for('admin.show_item_list')
    return redirect(next_page)


# categories management
@bp.route('/categories')
@login_required
def show_category_list():
    categories = Category.query.order_by('name').all()
    return render_template(
        'admin/rd_category.html',
        title='Categories',
        categories=categories
    )


@bp.route('/categories/create', methods=('GET', 'POST'))
@login_required
def create_category():
    form = CategoryForm()
    if form.validate_on_submit():
        category = Category.query.filter_by(name=form.name.data).first()
        if category is not None:
            flash('Please use a different category name')
            return render_template(
                'admin/cud_category.html',
                title='Create Category',
                form=form
            )
        category = Category(name=form.name.data)
        db.session.add(category)
        db.session.commit()
        flash('Category added')
        return redirect(url_for('admin.show_category_list'))
    return render_template(
        'admin/cud_category.html',
        title='Create category',
        form=form
    )


@bp.route('/categories/update/<int:category_id>', methods=('GET', 'POST'))
@login_required
def update_category(category_id):
    category = Category.query.get(category_id)
    if category is None:
        flash('No category with this id')
        return redirect(url_for('admin.show_category_list'))
    form = CategoryForm()
    if form.validate_on_submit():
        if form.name.data:
            category2 = Category.query.filter_by(name=form.name.data).first()
            if category2 is not None and category2.id != category.id:
                flash('Please use a different category name')
                return render_template(
                    'admin/cud_category.html',
                    title=category.name,
                    form=form,
                    id=category.id
                )
            category.name = form.name.data
        db.session.commit()
        flash('Category edited')
        return redirect(url_for('admin.show_category_list'))
    elif request.method == 'GET':
        form.name.data = category.name
    return render_template(
        'admin/cud_category.html',
        title=category.name,
        form=form,
        id=category.id
    )


@bp.route('/categories/delete/<int:category_id>')
@login_required
def delete_category(category_id):
    category = Category.query.get(category_id)
    if category is None:
        flash('No category with this id')
        return redirect(url_for('admin.show_category_list'))
    db.session.delete(category)
    db.session.commit()
    flash('Category deleted')
    return redirect(url_for('admin.show_category_list'))


@bp.route('/categories/delete', methods=('POST',))
@login_required
def delete_categories():
    for category_id in request.form:
        db.session.delete(Category.query.get(category_id))
    db.session.commit()
    return redirect(url_for('admin.show_category_list'))


# brands management
@bp.route('/brands')
@login_required
def show_brand_list():
    brands = Brand.query.order_by('name').all()
    return render_template(
        'admin/rd_brand.html',
        title='Brands',
        brands=brands
    )


@bp.route('/brands/create', methods=('GET', 'POST'))
@login_required
def create_brand():
    form = BrandForm()
    if form.validate_on_submit():
        brand = Brand.query.filter_by(name=form.name.data).first()
        if brand is not None:
            flash('Please use a different brand name.')
            return render_template(
                'admin/cud_brand.html',
                title='Create brand',
                form=form
            )
        brand = Brand(name=form.name.data)
        if form.image.data:
            brand.image = image_to_base64(form.image.data, (512, 512))
        db.session.add(brand)
        db.session.commit()
        flash('Brand added.')
        return redirect(url_for('admin.show_brand_list'))
    return render_template(
        'admin/cud_brand.html',
        title='Create brand',
        form=form
    )


@bp.route('/brands/update/<int:brand_id>', methods=('GET', 'POST'))
@login_required
def update_brand(brand_id):
    brand = Brand.query.get(brand_id)
    if brand is None:
        flash('No brand with this id.')
        return redirect(url_for('admin.show_brand_list'))
    form = BrandForm()
    if form.validate_on_submit():
        if form.name.data:
            brand2 = Brand.query.filter_by(name=form.name.data).first()
            if brand2 is not None and brand2.id != brand.id:
                flash('Please use a different brand name.')
                return render_template(
                    'admin/cud_brand.html',
                    title=brand.name,
                    form=form,
                    id=brand.id,
                    image=brand.image
                )
            brand.name = form.name.data
        if form.image.data:
            brand.image = image_to_base64(form.image.data, (512, 512))
        db.session.commit()
        flash('Brand edited.')
        return redirect(url_for('admin.show_brand_list'))
    elif request.method == 'GET':
        form.name.data = brand.name
    return render_template(
        'admin/cud_brand.html',
        title=brand.name,
        form=form,
        id=brand.id,
        image=brand.image
    )


@bp.route('/brands/delete/<int:brand_id>')
@login_required
def delete_brand(brand_id):
    brand = Brand.query.get(brand_id)
    if brand is None:
        flash('No brand with this id.')
        return redirect(url_for('admin.show_brand_list'))
    db.session.delete(brand)
    db.session.commit()
    flash('Brand deleted.')
    return redirect(url_for('admin.show_brand_list'))


@bp.route('/brands/delete', methods=('POST',))
@login_required
def delete_brands():
    for brand_id in request.form:
        db.session.delete(Brand.query.get(brand_id))
    db.session.commit()
    return redirect(url_for('admin.show_brand_list'))
