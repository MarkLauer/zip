from flask import render_template
from . import db


def forbidden(e):
    return render_template('errors/403.html'), 403


def page_not_found(e):
    return render_template('errors/404.html'), 404


def internal_error(e):
    db.session.rollback()
    return render_template('errors/500.html'), 500
