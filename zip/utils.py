import tempfile
import base64

from flask import Markup
from PIL import Image


def image_to_base64(file, size):
    image = Image.open(file)
    image.thumbnail(size)
    x, y = image.size
    if x / y > 1:
        y = int(max(y * size[0] / x, 1))
        x = int(size[0])
    elif x / y < 1:
        x = int(max(x * size[1] / y, 1))
        y = int(size[1])
    elif x / y == 1:
        x, y = size
    image = image.resize((x, y))
    back = Image.new('RGBA', size, (255, 255, 255, 1))
    back.paste(
        image,
        (
            int((size[0] - image.size[0]) / 2),
            int((size[1] - image.size[1]) / 2)
        )
    )
    temp = tempfile.TemporaryFile()
    back.save(temp, 'PNG')
    temp.seek(0)
    return 'data:{};base64,{}'.format(
        'PNG',
        base64.b64encode(temp.read()).decode('ascii')
    )


def unescape_newlines(text):
    result = ''
    for line in text.split('\n'):
        result += Markup.escape(line) + Markup('<br />')
    return result
