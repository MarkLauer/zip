# ZIP

## Начальная настройка

```sh
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
export FLASK_APP=zip
```

## Запуск

```sh
flask run
```

## Деплой

```sh
git pull
sudo supervisorctl stop zip
flask db upgrade # если поменял что-то в базе
sudo supervisorctl start zip
```

https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xvii-deployment-on-linux
