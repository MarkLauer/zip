"""
Special run script for PyCharm integration
"""
from zip import create_app

app = create_app()

if __name__ == '__main__':
    app.run(debug=True, use_debugger=False, use_reloader=False,
            threaded=False, passthrough_errors=True)
